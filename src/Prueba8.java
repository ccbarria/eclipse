import java.util.Scanner;
import javax.swing.JOptionPane;
public class Prueba8 {
	public static void main (String[] args) {
		Prueba8 llamada = new Prueba8();
        llamada.UsandoFor();
        llamada.UsandoArreglo();
}

public void UsandoFor() {
  Scanner entrada = new Scanner (System.in);
  
  for (int i = 0; i < 3; i++) {
	  System.out.print(i + " ");  
//	  JOptionPane.showMessageDialog(null, i);
	  JOptionPane.showMessageDialog(null, i,  "Numero es",JOptionPane.INFORMATION_MESSAGE);

  }
}



public void UsandoArreglo() {
	  Scanner entrada = new Scanner (System.in);
	  
	  int [] Enteros = new int[3];
	  double [] Decimales = new double[3];
	//  double[ ] numero = {2, 4, 6, 3.4, 6.7, 8.9}; //Array de 6 elementos
	  int num1 = 0;
	  double num2 = 0;
	  System.out.println("");
	  System.out.println("Ingrese 3 numeros entero: " );
	  for (int j = 0; j < 3; j++) {
		  num1 = entrada.nextInt();
		  Enteros [j] = num1;
	  }  
	  System.out.println("");
	  System.out.println("Ingrese 3 numeros decimales: " );
	  for (int j = 0; j < 3; j++) {
		  num2 = entrada.nextDouble();
		  Decimales [j] = num2;
	  } 
	  
	  System.out.println("");	  
	  for (int j = 0; j < 3; j++) {
		  JOptionPane.showMessageDialog(null, Enteros[j],  "Numero Entero",JOptionPane.INFORMATION_MESSAGE);
//		   System.out.print(Enteros[j] + " "); 
	   }
	  System.out.println("");	  
	  for (int j = 0; j < 3; j++) {
		  JOptionPane.showMessageDialog(null, Decimales[j],  "Numero Decimal",JOptionPane.INFORMATION_MESSAGE);
//			   System.out.print(Decimales[j] + " "); 
   }
	}
	}
